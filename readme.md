กาลครั้งหนึ่งนานมาแล้ว

มีหมาป่าตัวหนึ่งเดินเข้าไปในป่า

มีหนูน้อยหมวกดำ

กำลังตามหายายที่ไปเก็บเห็ด

เธอเป็นเด็กน้อยน่ารัก มีนัยตาสีฟ้า
ตอนนี้ฉันหิวข้าวมากๆเลย
ณ ลานดินแห่งหนึ่ง หนูกับมดสร้างรังอยู่ใกล้ ๆ กัน ต่อมาไม่นานเกิดฝนตกหนัก ทั้งรังหนูและรังมดที่อยู่ใต้ดิน ถูกน้ำท่วม ทำให้ฝูงมดจมน้ำตายไปเป็นจำนวนมาก ส่วนพวกหนูไม่เป็นอะไรเลย พวกมดสงสัยจึงถามหนูว่า "ทำไมพวกเจ้าถึงรอดจากน้ำท่วมมาได้ล่ะ" หนูจึงตอบว่า "ก็เพราะว่าพวกข้าทำทางเข้าบ้านไว้สองทางยังไงล่ะ พอทำท่วมทางแรก พวกเราก็รีบหนีออกไปทางที่สองได้ทัน"

 

:: นิทานเรื่องนี้สอนให้รู้ว่า ::

ควรเตรียมทางหนีทีไล่เผื่อไว้สำหรับในทุก ๆ เรื่อง

แต่ฉันยังไหวอยู่
# ขอเริ่มใหม่ครับ  ไม่ทัน
## nyaa~
# ขอเริ่มใหม่ครับ  ไม่ทัน
กาบครั้งสองนานมาแล้ว


# จบบริบูรณ์
# จบบริบูรณ์

ล้อเล่นน่าาาา ยังไม่จบหรอก
ฉันก็ยังหิวข้าวอยู่เหมือนเดิม

วันหนึ่งฉันเดินเข้าป่า
ฉันเจอนกตัวหนึ่ง มันถามฉันว่าจะไปไหน
ฉันจึงตอบ อยากไปให้ไกล
ไกลเกินกว่าที่ฉันเคยไป
โอ้โอ เฮ้อเออ



เราจะทำตามสัญญา ขอเวลาอีกไม่นาน
แล้วแผ่นดินที่งดงามจะคืนกลับมา
เราจะทำอย่างซื่อตรง ขอแค่เธอจงไว้ใจและศรัทธา
แผ่นดินจะดีในไม่ช้า ขอคืนความสุขให้เธอ ประชาชน




![](https://storage.thaipost.net/main/uploads/photos/big/20190125/image_big_5c4a707dad998.jpg)
test